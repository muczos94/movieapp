﻿using AutoMapper;
using Vidly.Dtos;
using Vidly.Models;
using Customer = Vidly.Models.Customer;

namespace Vidly
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Customer, CustomerDto>();
            Mapper.CreateMap<CustomerDto, Customer>()
                .ForMember(m => m.Id, o => o.Ignore());
            Mapper.CreateMap<Movie, MovieDto>();
            Mapper.CreateMap<MovieDto, Movie>()
                .ForMember(dest => dest.Id, s => s.Ignore());
        }
    }
}
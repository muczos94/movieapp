namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsertMovies : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT Movies ON");
            Sql("INSERT INTO Movies(Id, Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES (1, 'Shrek', 5, '2012-12-12', '1999-03-22', 10)");
            Sql("INSERT INTO Movies(Id, Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES (2, 'Meet My Family', 2, '2015-01-17', '2001-09-02', 2)");
            Sql("SET IDENTITY_INSERT Movies OFF");
        }

        public override void Down()
        {
        }
    }
}

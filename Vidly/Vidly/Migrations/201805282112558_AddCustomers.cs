namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCustomers : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT Customers ON");
            Sql("INSERT INTO Customers(Id, Name, BirthDate, IsSubscribedToNewsletter, MembershipTypeId) VALUES (1, 'Jakub Gerlee', '1994-12-12', 1, 1)");
            Sql("INSERT INTO Customers(Id, Name, BirthDate, IsSubscribedToNewsletter, MembershipTypeId) VALUES (2, 'Ela Durek', '1992-01-17', 1, 2)");
            Sql("SET IDENTITY_INSERT Customers OFF");

        }

        public override void Down()
        {
        }
    }
}
